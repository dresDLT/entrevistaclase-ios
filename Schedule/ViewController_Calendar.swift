//
//  ViewController_Calendar.swift
//  Schedule
//
//  Created by ANDRES DE LA TORRE on 20/6/17.
//  Copyright © 2017 Andrés de la Torre. All rights reserved.
//

import UIKit
import JTAppleCalendar

class ViewController_Calendar: UIViewController, JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource, UITableViewDataSource, UITableViewDelegate {
    
    //let dateFormatter = DateFormatter()

    @IBOutlet weak var calendarView: JTAppleCalendarView!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    
    let nameEvents = ["Dr. Andrews Apopointment"]
    let locationEvents = ["Nottingham Medical Center"]
    
    let dateFormatter = DateFormatter()
    
    
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        
        dateFormatter.dateFormat = "yyyy MM dd"
        dateFormatter.timeZone = Calendar.current.timeZone
        dateFormatter.locale = Calendar.current.locale
        
        let startDate = dateFormatter.date(from: "2017 01 01")!
        let endDate = dateFormatter.date(from: "2017 12 31")!
        
        let parameters = ConfigurationParameters(startDate: startDate, endDate: endDate)
        return parameters
        
    }
    
    //Muestra la Celda
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CalendarCell", for: indexPath) as! CalendarCell
        cell.dateLabel.text = cellState.text
        
        //Pintar celdas seleccionadas
        handleCellSelected (view: cell, cellState: cellState)
        //Pintar texto 
        handleCellTextColor(view: cell, cellState: cellState)
    
        return cell
    }
    
    //Seleccionar una Fecha
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        
        handleCellSelected (view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
        print("\n AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.string(from: date)
        print(dateFormatter.string(from: cellState.date))
    }
    
    //Des-Seleccionar una Fecha
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        
        handleCellSelected (view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
    }
    
    //función llamada para seleccionar o des-seleccionar una celda
    func handleCellSelected (view: JTAppleCell?, cellState: CellState){
        
        guard let validCell = view as? CalendarCell else{ return }
        
        if cellState.isSelected {
            validCell.selectedView.isHidden = false
        }else {
            validCell.selectedView.isHidden = true
        }
        
    }
    
    //funcion cambiar de color el texto
    func handleCellTextColor (view: JTAppleCell?, cellState: CellState){
        guard let validCell = view as? CalendarCell else{ return }
        
        if cellState.isSelected {
            validCell.dateLabel.textColor = .white
        }else {
            if cellState.dateBelongsTo == .thisMonth {
                validCell.dateLabel.textColor = .black
            } else {
                validCell.dateLabel.textColor = .gray
            }
        }
    }
    
    //Coger Mes y Año
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
    
        setupViewOfCalendar(from: visibleDates)
    }
    
    //Comienzo ---------------------------------------------------------------------
    func setupCalendarView(){
        
        calendarView.visibleDates { (visibleDates) in
            self.setupViewOfCalendar(from: visibleDates)
        }
        
    }
    
    func setupViewOfCalendar (from visibleDates: DateSegmentInfo) {
        let date = visibleDates.monthDates.first!.date
        
        self.dateFormatter.dateFormat = "yyyy"
        self.yearLabel.text = self.dateFormatter.string(from: date)
        
        self.dateFormatter.dateFormat = "MMMM"
        self.monthLabel.text = self.dateFormatter.string(from: date).uppercased()
    }
    //-------------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Colorear Status
        changeColorStatus(color: .orange, view: self.view)
        
        //Calendario
        self.calendarView.calendarDataSource = self
        self.calendarView.calendarDelegate = self
        setupCalendarView()
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("ENTRO AL WILL")
        self.view.alpha = 1
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("ENTRO AL DID")
        self.view.alpha = 1
    }
    
    //-------------------------------------------------------------------------------

//    @IBAction func menuButton(_ sender: UIBarButtonItem) {
//        //self.view.color
//        
//        performSegue(withIdentifier: "segueMenu", sender: self)
//    }
//    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        //let vc = segue.destination as! ViewController
//        //vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        self.view.alpha = 0.6
//    }
    
    //-------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "celdaEvents", for: indexPath) as! TableViewCell_SeeEvent
        
        cell.nameLabel.text = nameEvents[indexPath.row]
        cell.locationLabel.text = locationEvents[indexPath.row]
        cell.horaLabel.text = "19:45"
        
        return cell
        
    }
    
    
    
    
    
    
    

}
