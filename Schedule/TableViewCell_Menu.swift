//
//  TableViewCell_Menu.swift
//  Schedule
//
//  Created by ANDRES DE LA TORRE on 27/6/17.
//  Copyright © 2017 Andrés de la Torre. All rights reserved.
//

import UIKit

class TableViewCell_Menu: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBOutlet weak var opcion: UILabel!
    
    @IBOutlet weak var icono: UIImageView!
    

}
