//
//  ViewController.swift
//  Schedule
//
//  Created by ANDRES DE LA TORRE on 20/6/17.
//  Copyright © 2017 Andrés de la Torre. All rights reserved.
//

import UIKit
import JTAppleCalendar

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var celdaSeleccionada:Int?
    
    
    @IBOutlet weak var tap1: UITapGestureRecognizer!
    
    @IBOutlet weak var viewBlue: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Colorear Status
        let statusBarColor = viewBlue.backgroundColor
        let colorStatusBAr : UIView = UIView(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
        colorStatusBAr.backgroundColor = statusBarColor
        self.view.addSubview(colorStatusBAr)
        
        //Gesto
        tap1.numberOfTouchesRequired = 1
        
        //
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return home.count
        case 1:
            return trackers.count
        case 2:
            return features.count
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuID") as! TableViewCell_Menu
        
        switch indexPath.section {
        case 0:
            cell.opcion.text = home[indexPath.row]
            
//            guard let paisBandera = banderaDiccinario [paisAmerica[indexPath.row]] else {
//                celda.imagenBandera.image = #imageLiteral(resourceName: "noHayBandera.png")
//                return celda
//            }
//            celda.imageView?.image = paisBandera
            
        case 1:
            cell.opcion.text = trackers[indexPath.row]
        case 2:
            cell.opcion.text = features[indexPath.row]
        default:
            cell.textLabel?.text = ""
        }
        //
        
        return cell
        
        
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
            case 0:
                return "Home"
            case 1:
                return "Trackers"
            case 2:
                return "Features"
            default:
                return ""
        }
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        celdaSeleccionada = indexPath.row
        return indexPath
    }
    
    
    @IBAction func tapOnView(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
        
    }
    

}

