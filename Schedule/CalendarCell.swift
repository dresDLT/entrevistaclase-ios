//
//  CalendarCell.swift
//  Schedule
//
//  Created by ANDRES DE LA TORRE on 20/6/17.
//  Copyright © 2017 Andrés de la Torre. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CalendarCell: JTAppleCell {

    @IBOutlet weak var dateLabel: UILabel!

    @IBOutlet weak var selectedView: UIView!
}
