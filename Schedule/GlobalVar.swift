//
//  GlobalVar.swift
//  Schedule
//
//  Created by ANDRES DE LA TORRE on 20/6/17.
//  Copyright © 2017 Andrés de la Torre. All rights reserved.
//

import UIKit
import Foundation
//import JTAppleCalendar


let home = ["Diary"]
let trackers = ["Log Food Entry", "Weight Tracker", "Mood Tracker", "Medication Tracker", "Symptom Tracker"]
let features = ["Schedules", "Goals", "Resources"]

//colorear Status
func changeColorStatus (color: UIColor, view: UIView){
    let statusBarColor = color
    let colorStatusBAr : UIView = UIView(frame: CGRect.init(x: 0, y: 0, width: view.frame.size.width, height: 20))
    colorStatusBAr.backgroundColor = statusBarColor
    view.addSubview(colorStatusBAr)
}
