//
//  TableViewCell_SeeEvent.swift
//  Schedule
//
//  Created by ANDRES DE LA TORRE on 29/6/17.
//  Copyright © 2017 Andrés de la Torre. All rights reserved.
//

import UIKit

class TableViewCell_SeeEvent: UITableViewCell {
    
    
    @IBOutlet weak var horaLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
