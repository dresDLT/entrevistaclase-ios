//
//  TableViewCell_Event.swift
//  Schedule
//
//  Created by ANDRES DE LA TORRE on 29/6/17.
//  Copyright © 2017 Andrés de la Torre. All rights reserved.
//

import UIKit

class TableViewCell_Event: UITableViewCell {
    
    @IBOutlet weak var tittleLabel: UILabel!

    @IBOutlet weak var informationText: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
