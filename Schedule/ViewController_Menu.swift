//
//  ViewController_Menu.swift
//  Schedule
//
//  Created by ANDRES DE LA TORRE on 22/6/17.
//  Copyright © 2017 Andrés de la Torre. All rights reserved.
//

import UIKit

class ViewController_Menu: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        //Colorear Status
        let statusBarColor = UIColor.orange
        let colorStatusBAr : UIView = UIView(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
        colorStatusBAr.backgroundColor = statusBarColor
        self.view.addSubview(colorStatusBAr)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
