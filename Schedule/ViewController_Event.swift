//
//  ViewController_Event.swift
//  Schedule
//
//  Created by ANDRES DE LA TORRE on 22/6/17.
//  Copyright © 2017 Andrés de la Torre. All rights reserved.
//

import UIKit

class ViewController_Event: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    
    @IBOutlet weak var eventName: UINavigationBar!
    
    let titleLista = ["Event type","Location","Date","Time","Reminder","Notes"]
    let informationLista = ["<event type>","Nottingham Medical Center","<date>","19:45","<reminder>","<Notes>"]

    override func viewDidLoad() {
        super.viewDidLoad()

        //Colorear Status
        changeColorStatus(color: .orange, view: self.view)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func backButton(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "celdaEvent", for: indexPath) as! TableViewCell_Event
        
        cell.tittleLabel.text = titleLista[indexPath.row]
        cell.informationText.text = informationLista[indexPath.row]
        
        eventName.topItem?.title = "Dr. Andrews Apopointment"
        
        return cell
        
    }
}
