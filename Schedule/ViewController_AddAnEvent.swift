//
//  ViewController_AddAnEvent.swift
//  Schedule
//
//  Created by ANDRES DE LA TORRE on 21/6/17.
//  Copyright © 2017 Andrés de la Torre. All rights reserved.
//

import UIKit

class ViewController_AddAnEvent: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate  {
    
    let titleLista = ["Event type","Event name","Location","Date","Time","Reminder","Notes"]
    
    var contador : Int = 0;
    
    @IBOutlet weak var saveItem: UIBarButtonItem!
    
    @IBOutlet weak var yConstrain_TableView: NSLayoutConstraint!
    
    @IBOutlet var tap1: UITapGestureRecognizer!

    override func viewDidLoad() {
        super.viewDidLoad()

        //Colorear Status
        changeColorStatus(color: .orange, view: self.view)
        
        //Notifications - Pa subir view cuando aparezca el teclado
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController_AddAnEvent.keyboardUP(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController_AddAnEvent.keyboardDOWN(notification:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
        
        //Gesto
        //tap1.numberOfTapsRequired = 1
        tap1.numberOfTouchesRequired = 1
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Change Zise of TableView
    func keyboardUP(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if contador == 0 {
                yConstrain_TableView.constant -= keyboardSize.height
                contador = 1;
            }
        }
    }
    
    func keyboardDOWN(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            if contador == 1 {
                yConstrain_TableView.constant += keyboardSize.height
                contador = 0
            }
        }
    }
    
    //-------------------------------------------------------------------------
    //PRIMERA FORMA
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //SEGUNDA FORMA
    @IBAction func tapOnView(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    //-------------------------------------------------------------------------
    
    
    //Propiedades de la Tabla
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleLista.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "celdaAddAnEvent", for: indexPath) as! TableViewCell_AddAnEvent
        
        cell.tittleLabel.text = titleLista[indexPath.row]
        
        return cell
        
    }
    

    @IBAction func saveItemPressed(_ sender: UIBarButtonItem) {
        print("EEEEEEEEEEEEEEEEEEEEE")
        
    }
    
    
    @IBAction func cancelButton(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
