//
//  ViewController_EditEvent.swift
//  Schedule
//
//  Created by ANDRES DE LA TORRE on 22/6/17.
//  Copyright © 2017 Andrés de la Torre. All rights reserved.
//

import UIKit

class ViewController_EditEvent: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let titleLista = ["Event type","Event name","Location","Date","Time","Reminder","Notes"]
    let informationLista = ["<event type>","Dr. Andrews Apopointment","Nottingham Medical Center","<date>","19:45","<reminder>","<Notes>"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Colorear Status
        changeColorStatus(color: .orange, view: self.view)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func cancelButton(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //Propiedades de la Tabla
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleLista.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "celdaEditEvent", for: indexPath) as! TableViewCell_AddAnEvent
        
        cell.tLabel.text = titleLista[indexPath.row]
        cell.iLabel.text = informationLista[indexPath.row]
        
        return cell
        
    }
}
